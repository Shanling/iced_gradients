//! For creating a Gradient.

use bytemuck::{Pod, Zeroable};
use iced_native::Color;
use crate::widget::canvas::gradient::Linear;

#[derive(Debug, Clone, PartialEq)]
/// A fill which transitions colors progressively along a direction, either linearly, radially,
/// or conically.
pub enum Gradient {
    /// A linear gradient interpolates colors along a direction from its [`start`] to its [`end`]
    /// point.
    Linear(Linear),
}


#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Zeroable, Pod)]
/// A point along the gradient vector where the specified [`color`] is unmixed.
pub struct ColorStop {
    /// Offset along the gradient vector.
    pub offset: f32,
    /// The color of the gradient at the specified [`offset`].
    pub color: [f32; 4],
    _padding: [u32; 3]
}

impl From<(f32, Color)> for ColorStop {
    fn from((offset, color): (f32, Color)) -> Self {
        Self {
            offset,
            color: color.into_linear(),
            _padding: [0; 3]
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Zeroable, Pod)]
/// Input parameters for all gradients.
pub struct Uniforms {
    ///The starting point of the gradient.
    pub start: [f32; 2],
    ///The ending point of the gradient.
    pub end: [f32; 2],
    ///The index of the first color stop in the gradient.
    pub start_stop: i32,
    ///The index of the last color stop in the gradient.
    pub end_stop: i32,
    /// Extra 8 bytes of padding to make 96 total
    pub _padding: [f32; 2]
}